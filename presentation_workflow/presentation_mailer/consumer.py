import json
import queue
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def process_approval(ch, method, properties, body):
            print("processing your data")
            data = json.loads(body)
            send_mail("Your presentation has been accepted", f'{data["presenter_name"]}, we are happy to tell you that your presentation {data["title"]} has been accepted', 'admin@conference.go', [data['presenter_email']], fail_silently=False)

        def main():
            print("inside main function")
            parameters = pika.ConnectionParameters(host='rabbitmq')
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()

            # CODE FOR PUB/SUB
            channel.exchange_declare(exchange='logs', exchange_type='fanout')
            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='logs', queue=queue_name)

            # CODE FOR QUEUES
            # channel.queue_declare(queue='tasks')
            # channel.basic_consume(
            #     queue='tasks',
            #     on_message_callback=process_approval,
            #     auto_ack=True,
            # )
            print(' [*] Waiting for messages. To exit press CTRL+C')
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=process_approval,
                auto_ack=True
            )
            channel.start_consuming()
        if __name__ == '__main__':
            try:
                main()
            except KeyboardInterrupt:
                print('Interrupted')
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)



# Just extra stuff to do when the script runs
